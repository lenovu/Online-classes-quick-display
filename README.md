# Online-classes-quick-display

💻 **Welcome to "OCQD".** 💻

__What is this project usefull for?:__
> 1.) Easily access online meeting data, to connect to the meeting, fast and easy. 💕

> 2.) Flex on others using different options like: "notepad, torn paper, memory" 💪

> 3.) Why not? 🤷‍♂️

❔ **FAQ** ❔

__How to acquire the code?__
> 1.) For Linux: Use "git clone" command. 🐧

> 2.) For Windows: Just manually "download" the file. 📺

__If you have any questions related to copyright, just read the "License" file.__





